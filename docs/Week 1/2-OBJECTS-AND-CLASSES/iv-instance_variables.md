# Instance and Class variables

For now we will dwell much in instance variable and discuss class variable, we just mentioned
class variable to let you know it exist(in short am telling you dig deep into what class variable)

suppose we have cheque which matures after 1 day, absurd this our dream land
who care.We to create cheque and the object must have the ability to remember the cheque was written
so that after that 1 day it matures.This is where instance variable kicks in it denote by @
remember our @bank_name yep that was instance variable.

- Let make this cheque mature 

```ruby
require 'time'

class Bank 
    
    def cheque_creation_time(cheque_creation_time)
      @cheque_creation_time = cheque_creation_time    
    end
    def cheque_mature?       
        if get_time_difference() >= 259200 
            "Yes"
        else
            "Patience My Friend"
        end
    end
    
    private
    def get_time_difference
        creation_time = Time.parse(@cheque_creation_time)
        current_time = Time.now
        current_time -  creation_time 
    end
end
```
Any object created and given creation time will remember it creation time,and that
the essence of instant variable memory.

You might seen strange words like require and private,private is keyword to used in scope to indicate that whatever is below in should not be used outside that scope. Scope is generally a block of related code where there operation are valid for example methods defined with a class are only are only valid with a class, variables defined with a method are only valid with that method.

write the above code using a attribute_accessor instead..break up things get those
errors it  is fun