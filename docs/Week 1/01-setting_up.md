# Setting Up
For this week we need setup our working environment by:
- installing ruby
- installing rspec
- installing editor

For now we will install the editor and ruby, rspec will install as we get into with the course, during
Test Driven Development face.

### Installing ruby
There are different ways of installing ruby depending on the OS for this reason am going to
point to the ruby official documentaton and rvm(ruby version manager) website. RVM is used to run different version of ruby it comes handy when you working on different projects that run on different versions of ruby. We reccomend using RVM since in future you will find yourself working with more than one ruby version.

- [installing ruby official docs](https://www.ruby-lang.org/en/documentation/installation/#apt)
- [using RVM](#using-rvm)

#### Using RVM {#using-rvm}
If you are using Ubuntu then you can follow the official instructions given on [this github page](https://github.com/rvm/ubuntu_rvm)

##### Note
This is the first external link with instructions we are giving you. Make sure you read the whole instruction and follow it step by step

### Install Editor
Well this the wide range of option I will not limit you to any but am going to give some options to look at
- [visual studio](https://code.visualstudio.com/download)
- [atom](https://flight-manual.atom.io/getting-started/sections/installing-atom/)
- [sublime text](https://www.sublimetext.com/3)

Okay, I lied when I said I will not limit you, can we just use visual studio just because it is the hottest thing right now :smiley:

Also have a look at VIM when you have time, it is the best editor (Dont try to use it just yet, lets stick to vscode)

![Quiting Vim as a beginner](img/quit_vim.jpg)
