# Introduction

##  Variable Declaration

Variable by definition is a placeholder.That is it sit in place of actual thing in this case a literal which is a datatype.

In JS variable declaration occurs in two phases:

- The declaration

- the iniatialization 

### the declaration

This is the phase which we let javascript know the variable exists and we want to use it.

We use key words : `let` , `const`, `var` (we will  dive deep  later) to achieve this

For this instance `let x;` or `const x;` or `var x;`.By now you have declared
the variable but note give them any value yet.Therefore they ar still undefined.
We can give the above variable value.This the second phase of variable declaration.

### the initialization

the declaration above can given value like so `x =4`.This is what is known as initialization.

## Let, var and const

[difference between let, var and const](https://medium.com/podiihq/javascript-variables-should-you-use-let-var-or-const-394f7645c88f)


## Javascript Statements
 
A JavaScript program is a list of programming statements.
JavaScript statements are composed of Values, Operators, Expressions, Keywords, and Comments.
The statements are executed, one by one, in the same order as they are written.
Semicolons separate JavaScript statements.Add a semicolon at the end of each executable statement:
 
Examples:
 
var a, b, c;     // Declare 3 variables
a = 5;           // Assign the value 5 to a
b = 6;           // Assign the value 6 to b
c = a + b;       // Assign the sum of a and b to c
 
NOTE: When separated by semicolons, multiple statements on one line are allowed:
a = 5; b = 6; c = a + b;
JavaScript ignores multiple spaces. You can add white space to your script to make it more readable, for instance, the following lines are equivalent:
 
var person = " Getty ";
var person="Getty";
 
A good practice is to put spaces around operators ( = + - * / ):
var x = y + z;
If a JavaScript statement does not fit on one line, the best place to break it is after an operator:
document.getElementById("demo").innerHTML =
"Hello Dolly!";
Javascript Code Blocks
JavaScript statements can be grouped together in code blocks, inside curly brackets {...}. The purpose of code blocks is to define statements to be executed together.
One place you will find statements grouped together in blocks, is in JavaScript functions:
 
 
 
```js
function myFunction() {
   .console.log("My name")
}
```

### Javascript Keywords

JavaScript statements often start with a keyword to identify the JavaScript action to be performed. A few examples are:
*break*, *continue*, *do* ... *while*, *function*, *return*
Javascript Syntax
Here are the most important things to note about Javascript syntax:
JavaScript syntax defines two types of values: Fixed values(literals) and variable values(variables).
Fixed values are two. Numbers and Strings:
Numbers are written with or without decimals:   10.5 , 1000, 1.0
Strings are text, written within double or single quotes:    “Getty”, ‘Gee’
Variable Values
variables are used to store data values. JavaScript uses the var keyword to declare variables. An equal sign(=) is used to assign values to variables.
`var x;`

`x = 6;`
JavaScript uses arithmetic operators ( + - *  / ) to compute values
JavaScript uses an assignment operator ( = ) to assign values to variables
An expression is a combination of values, variables, and operators, which computes to a value. The computation is called an evaluation .For example, 5 * 10 evaluates to 50
Not all JavaScript statements are "executed". Code after double slashes // or between /* and */ is treated as a comment. Comments are ignored, and will not be executed:
All JavaScript identifiers are case sensitive. The variables lastName and lastname, are two different variables.
For javascript variable names you can separate two words in the identifier using 1) underscore(first_name), hyphens(first-name), upper Camel Case (FirstName), Lower Camel Case(firstName)
Javascript Data Types
Javascript datatypes include, Number, strings, arrays, objects, booleans. You can use the JavaScript typeof operator to find the type of a JavaScript variable e.g
`typeof "John Doe"`  // Returns "string"

`typeof (3 + 4)`    // Returns "number"
In JavaScript, a variable without a value, has the value undefined
An empty string has both a legal value and a type.
In JavaScript null is "nothing". It is supposed to be something that doesn't exist. Unfortunately, in JavaScript, the data type of null is an object.
A primitive data value is a single simple data value with no additional properties and methods e.g  string, number, boolean, undefined 
A complex data value is a collecion of primitive values setup to accomplish something e.g function, object

### Javascript Objects

Objects are variables too. But objects can contain many values. The values are written as name:value pairs (name and value separated by a colon).

```js
var car = {type:"Fiat", 
model:"500", 
color:"white"};
```
Spaces and line breaks are not important. An object definition can span multiple lines
The name:values pairs in JavaScript objects are called properties
You can access object properties in two ways
objectName.propertyName or objectName["propertyName"]
Objects can also have methods. Methods are actions that can be performed on objects and are stored in properties as function definitions.
E.g
```js
fullName : function() {
     return this.firstName + " " + this.lastName;
 }
```

### Exercise

Create a file to do this exercise

1. Determine which datatype is:

 - [1,2,3,4]
 - "helloworld"
Using MDN built-in objects [guides](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)

2. Convert "with love" to upper case
3. Square root of 64
4. Find first element in [1, 2, 3, 4, 5]
5. Insert "last" at last index of ["rained", "summer", "winter"]


     + [1,2,3,4]
     
     + "helloworld"

2. Using MDN built-in objects [guides](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)

    + Convert "with love" to upper case

    + Square root of 64
    +  Find first element in [1, 2, 3, 4, 5]
    + Insert "last" at last index of ["rained", "summer", "winter"]
 
