# Objectives

## Undertanding the DOM 

 DOM is the getway to manipulation of html elements and obtain 
 the scripting power of Javascript.

 We will go through in practical and understand how follow the 
 DOM from top most parent to lowest child.

 By the end of it all you should be able to manipulate DOM content
 with easy to suit your need

### Events and Event Listeners
 
 To enhance UX the browser must react according to his or 
 her action.For instance if user click `download` button she
 expect a response wether negative or positive.
 We will understand how to allow user to interact with
 browser capture their action and give appropriate feedback.

 We will achieve by looking  deep into event and event listener


### Understanding control flow

 Just like the name suggest it controls the flow, that interrupts
 normal functioning of a programming by skipping or repeating  or doing
 anything that is not normal to a step.The helps in decision controls 
 which apply programming our daily life experience, where conditions or decisions are involved.


### Functions and loops

Functions are one of core concepts of any programming language the give 
immense as the allow reuseability.We will focus majorly on `ES6` way of
writing function(the arrow function) but we will also take a look at 
the named function.
Apart from user defined function will also take a look at *msdn* for
inbuilt js function and how the can easen our work.

We will we also take a look at the loops and how

## Basic Data type and operators

Data as you have already learnt is the core of programming, understanding
different operation which can be performed on different data as
manupulation to achieve our desired output will be one of key pillars 
of this week 

Student should understand basic js datatypes and our and manipulate them.

We spend sometime understanding different types of operator including
but not limited to logical operators, assignment operators among others
and see how the affect or help with decision making


