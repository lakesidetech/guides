# Operators

If the key does not have a data-action attribute, it must be a number key.

If the key has a data-action that is either add, subtract, multiply or divide, we know the key is an operator.

add the following in your *js*  just below the code you just wrote

```js
if (
  action === 'add' ||
  action === 'subtract' ||
  action === 'multiply' ||
  action === 'divide'
) {
  console.log('operator key!')
}
```

If the key’s data-action is decimal, we know the user clicked on the decimal key.
Notice the usage of || this a known or operator which is a logical operator.There several operator in javascripts [checkout operators](https://www.w3schools.com/js/js_comparisons.asp) 

Following the same thought process, if the key’s data-action is clear, we know the user clicked on the clear (the one that says AC) key. If the key’s data-action is calculate, we know the user clicked on the equals key

```js
if (action === 'decimal') {
  console.log('decimal key!')
}

if (action === 'clear') {
  console.log('clear key!')
}

if (action === 'calculate') {
  console.log('equal key!')
}
```



Open the *devtools* as you did previously and press the keys on o calculator and observe what happens on the console.

![display](../img/display.gif)