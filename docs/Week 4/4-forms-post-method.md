# Forms and post methods

In rare case will the user visiting a page hardcore input the url.There many ways to get inputs for user and one of them is form.Since it the common method will take look into it.

Let us assume when calculating sum of user input on url `/2/2` your route was 
`/sum/:first_number/:second` with that assumption in mind(if you used another routes it ok just change content to fit your app).

**Change the content of route like**

Note the removable of variables of the route

*myapp.rb*
```ruby
#some code above this comment
get '/sum' do

    @first_number =params[:first_number]
    @second_number =params[:second_number]
    erb:sum # use whatever template used for this

end
## some routes below this comment

```

Remove the content  on  the template you created to display sum result and replace with this form and other content.In my case I will use `sum.erb`.

My assumption here is your created an erb file inside view directory
*sum.erb*

```html
<h3>Enter Your Values</h3>
<form>
     <input name="first_number" type="text" placeholder="First Number" ><br>
     <input name="second_number" type="text" placeholder="Second Number"><br>
     <input type="submit" value="calculate">     
    
</form>
 <p>The sum of <%=@first_number%> and <%=@second_number%> is <%= @first_number.to_f + @second_number.to_f%></p>
```

This is template allow user to enter their data via form and see result after calculation

## Conditional Rendering

It is awkward that our form show us results even without entering.Let us deal with that.When you display only what you want be seen by user on browser when certain condition is met is known as conditional rendering.We can you control
flow statement for this.

*sum.erb*

```html 

<h3>Enter Your Values</h3>
<form>
     <input name="first_number" type="text" placeholder="First Number" ><br>
     <input name="second_number" type="text" placeholder="Second Number"><br>
     <input type="submit" value="calculate">    
</form>
 <% if @first_number != nil and @second_number != nil %>
         <%if @first_number.strip == "" or @second_number.strip==""%>
             <p> Kindly fill the form to calculate </p>
              <%else%>
              <p>The sum of <%=@first_number%> and <%=@second_number%> is <%= @first_number.to_f + @second_number.to_f%></p>
        
         <%end%>
     
     <%end%>
``` 

When the form is first rendered first_name and second_name have values of `nil` since we have not yet passed any data.Therefore result will not be render unless we hit calculate.But when we hit calculate with our form blank or if its content are spacebar or tab created param, we display `Kindly fill the form to calculate` since `strip` method remove all leading or trailing on a string the we compare value to empty string if true we request user to fill form. 

If we enter value and click calculate we get the sum but there is catch when user enter no numeric value is give us result logically that's absurd but for now bare with me I wanna take deteour.We shall revisit soon.

## Rendering of different template

When we calculate  the sum you realize that we  the result on the  same page/template.Well that's not the only way out and to be honest it was rather hectic
when render because we had to check whether our params were  empty string. Form
are ussually give action which the routes the are to render at if not given like we did not give our on the previous example it renders on the same  page.Let us add an action which will hit `/calculation_results` in our application.Add it like so:

*sum.erb*

```html 

<h3>Enter Your Values</h3>
<form action="/calculation_results">
     <input name="first_number" type="text" placeholder="First Number" ><br>
     <input name="second_number" type="text" placeholder="Second Number"><br>
     <input type="submit" value="calculate">
     
</form>
<% if @first_number != nil and @second_number != nil %>
         <%if @first_number.strip == "" or @second_number.strip==""%>
             <p> Kindly fill the form to calculate </p>
              <%else%>
              <p>The sum of <%=@first_number%> and <%=@second_number%> is <%= @first_number.to_f + @second_number.to_f%></p>
        
         <%end%>
     
     <%end%>
```

When we enter our values and try calculating error pops on our face.Route /`calculation_results` is not there.Let us create in our application like so:

*myapp.rb*

```ruby
# other routes
# /sum route
get "/calculation_results"
erb: results
end
#other routes


```
This demands we create `results` template kindly do so.

This new addition also demands refactor from in `/sum` route block.We longer receive params there it's prime function is just now to render our form therefore:

Edit content of `/sum` routes like so.

*myapp.rb*

```ruby
#other routes
get '/sum' do
    erb:sum
end
# /calculate_results route
```

We also have to factor `sum.erb` file but remove all content outside the form tag.Cut them and copy them in `result.erb` you created.One more refactor is required.We are not handling params coming to `calculate_results` let us to that.

```ruby
# other routes
# /sum route

get "/calculation_results" do
  @first_number =params[:first_number]
  @second_number =params[:second_number]  
end
 # other routes
```
This if ` <% if @first_number != nil and @second_number != nil %>` statement in `result.erb` do serve us anymore There no way user will send nil value anymore, we have seperate the forms. Remove it


## Validations  

What if user enter wrong input for instance in sum route we expect only integer,the user enter non-numeric input for now application just to convert
whatever the input to float and since non-numeric converted to interger or float returns 0.This should not be the case we shoud be case and I promised will visit well now this is the time.


```ruby

```




## post method



